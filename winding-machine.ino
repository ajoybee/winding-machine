/*
线轴自动绕线算法。
根据线轴长度、线径、绕线层数，计算经向电机旋转圈数，以及送线电机旋转圈数。
*/
#include <AccelStepper.h>  //本示例程序使用AccelStepper库

// 步进电机和滑台的参数
const long steps_per_round = 1600;  // 步进电机每圈步数
const long distance_per_round = 4;  // 滑台每转动一圈前进的距离，单位毫米

// 电机控制用常量
const int enable_pin = 8;  // 使能控制引脚
const int x_dir_pin = 5;   // x方向控制引脚
const int x_step_pin = 2;  // x步进控制引脚
const int y_dir_pin = 6;   // y方向控制引脚
const int y_step_pin = 3;  // y步进控制引脚

// ==================== 绕线参数，针对0.48线径 ====================
struct {
  const long spool_length = 24;      // 线轴长度，单位毫米
  const float wire_diameter = 0.49;  // 线径，在实际测量线径基础上增加10%，作为绕线间隙的宽度。
  // const long spool_length = 15;                                                                                  // 线轴长度，单位毫米
  // const float wire_diameter = 0.36;                                                                              // 线径，在实际测量线径基础上增加10%，作为绕线间隙的宽度。
  // const long spool_length = 15;      // 线轴长度，单位毫米。设置比实际测量减少 1-2 毫米
  // const float wire_diameter = 0.49;  // 线径，在实际测量线径基础上增加 0.02 毫米，作为绕线间隙的宽度。
  const long wiring_layer = 12;       // 绕线层数
  const long wiring_speed = 1600;    // 绕线速度，步/秒
  const long regulate_speed = 1600;                                                                              // 调整速度，步/秒
  const long sync_wheel_ratio = 4;                                                                               // 同步轮传动比
  const long wiring_steps = (spool_length / wire_diameter) * wiring_layer * steps_per_round * sync_wheel_ratio;  // 绕线电机总步数
  const long feed_speed = (wiring_speed / sync_wheel_ratio) * wire_diameter / distance_per_round;                // 送线速度
  const long one_way_steps = steps_per_round * spool_length / distance_per_round;                                // 送线电机单程步数
  const long regulate_delta = steps_per_round / 8;                                                               // 单次调整的增量，按照每圈步数的1/8计算
} wiring_profile;

struct {
  int current_status = 0;         // 绕线状态：0 待机；1 前向绕线；2 后向绕线；3 前向暂停；4 后向暂定；9 完成
  long current_position = 0;      // 送线电机当前位置
  char control_command;           // 指令字符
  boolean command_pulse = false;  // 控制指令标志，解析控制指令过程中一次生效
  int offset_multiple = 1;            // 单次调整增量的倍数
} wiring_status;

AccelStepper stepper1(1, x_step_pin, x_dir_pin);  // 绕线电机
AccelStepper stepper2(1, y_step_pin, y_dir_pin);  // 送线电机e

void setup() {
  pinMode(x_step_pin, OUTPUT);    // Arduino控制A4988x步进引脚为输出模式
  pinMode(x_dir_pin, OUTPUT);     // Arduino控制A4988x方向引脚为输出模式
  pinMode(y_step_pin, OUTPUT);    // Arduino控制A4988y步进引脚为输出模式
  pinMode(y_dir_pin, OUTPUT);     // Arduino控制A4988y方向引脚为输出模式
  pinMode(enable_pin, OUTPUT);    // Arduino控制A4988使能引脚为输出模式
  digitalWrite(enable_pin, LOW);  // 将使能控制引脚设置为低电平从而让电机驱动板进入工作状态

  stepper1.setMaxSpeed(wiring_profile.wiring_speed);      // 绕线电机最大速度
  stepper1.setAcceleration(wiring_profile.wiring_speed);  // 绕线最大加速度
  stepper2.setMaxSpeed(wiring_profile.feed_speed);        // 送线电机最大速度，与线径和滑台每圈步长的比值有关
  stepper2.setAcceleration(wiring_profile.feed_speed);    // 送线电机加速度

  Serial.begin(38400);
  Serial.println(F("++++++++++++++++++++++++++++++++++"));
  Serial.println(F("+           自动绕线机            +"));
  Serial.println(F("++++++++++++++++++++++++++++++++++"));
  Serial.println(F(""));
  Serial.println(F("Please input your command:"));
}

void loop() {
  parse_cmd();
  run_cmd();
}

void parse_cmd() {
  if (Serial.available()) {                         // 检查串口缓存是否有数据等待传输
    wiring_status.control_command = Serial.read();  // 获取指令中
    Serial.print(F("cmd = "));
    Serial.println(wiring_status.control_command);
    switch (wiring_status.control_command) {
      case 'p':  // 暂停运行
        break;

      case 'r':  // 继续运行
        parse_resume();
        break;

      case 'f':  // 前向送线
        parse_offset();
        break;

      case 'b':  // 后向送线
        parse_offset();
        break;

      default:  // 未知指令
        Serial.println(F("Unknown Command"));
    }
  }
}

// 解析绕线指令
void parse_resume() {
  int resume_status = Serial.parseInt();
  if (resume_status == 1 || resume_status == 2) {  // 有效输入绕线指令是 1 或 2
    Serial.print(F("resume = "));
    Serial.println(resume_status);
    wiring_status.current_status = resume_status;
  } else {
    Serial.print(F("default resume = "));
    Serial.println(1);  // 默认前向绕线
    wiring_status.current_status = 1;
  }
  if (wiring_status.current_status == 1) {  // 如果绕线指令是前向绕线
    stepper2.setCurrentPosition(0);
  } else {
    stepper2.setCurrentPosition(wiring_profile.one_way_steps);
  }
}

// 解析偏移量指令
void parse_offset() {
  wiring_status.command_pulse = true;
  int offset = Serial.parseInt();
  if (offset > 0) {
    Serial.print(F("offset = "));
    Serial.println(offset);
    wiring_status.offset_multiple = offset;
  } else {
    Serial.print(F("default offset = "));
    Serial.println(1);  // 默认 1 倍偏移量
    wiring_status.offset_multiple = 1;
  }
}

void run_cmd() {
  if (wiring_status.current_status == 0) {  // 待机状态
    if (wiring_status.control_command == 'r') {
      resume();
    } else if (wiring_status.control_command == 'f') {
      regulate_forward();
    } else if (wiring_status.control_command == 'b') {
      regulate_backward();
    }
  } else if (wiring_status.current_status == 1 || wiring_status.current_status == 2) {  // 前向绕线或后向绕线
    if (wiring_status.control_command == 'p') {
      wiring_status.current_status = wiring_status.current_status + 2;  // 状态变成前向暂停或后向暂停
      pause();
    } else {
      resume();
    }
  } else if (wiring_status.current_status == 3 || wiring_status.current_status == 4) {  // 前向暂停或后向暂停
    if (wiring_status.control_command == 'r') {
      wiring_status.current_status = wiring_status.current_status - 2;  // 状态变成前向绕线或后向绕线
      resume();
    } else if (wiring_status.control_command == 'f') {
      regulate_forward();
    } else if (wiring_status.control_command == 'b') {
      regulate_backward();
    }
  }
}

// 绕线电机持续绕线
void resume() {
  // 绕线电机运行指定圈数
  long remaining_steps = wiring_profile.wiring_steps - stepper1.currentPosition();
  if (remaining_steps > steps_per_round) {
    stepper1.move(steps_per_round);
    back_and_forth();
    run_all();
  } else if (remaining_steps > 0) {
    stepper1.move(remaining_steps);
    back_and_forth();
    run_all();
  } else if (remaining_steps == 0) {
    stepper2.stop();                   // 如果绕线电机剩余步数为0，则送线电机停止
    stepper1.setCurrentPosition(0);    // 设置当前位置为0
    stepper2.setCurrentPosition(0);    // 设置当前位置为0
    wiring_status.current_status = 9;  // 状态变成完成
  }
}

// 送线电机往复运动
void back_and_forth() {
  if (stepper2.currentPosition() > 0 && stepper2.currentPosition() < wiring_profile.one_way_steps) {  // 如果送线点在端点之间
    if (wiring_status.current_status == 1) {                                                          // 当状态是前向绕线
      stepper2.moveTo(wiring_profile.one_way_steps);
    } else {
      stepper2.moveTo(0);
    }
  } else if (stepper2.currentPosition() == 0) {  // 如果送线到达初始端点
    stepper2.moveTo(wiring_profile.one_way_steps);
    wiring_status.current_status = 1;                                       // 状态变成前向绕线
  } else if (stepper2.currentPosition() == wiring_profile.one_way_steps) {  // 如果送线到达最大端点
    stepper2.moveTo(0);
    wiring_status.current_status = 2;  // 状态变成后向绕线
  }
}

// 持续运行
void run_all() {
  stepper1.run();  // 绕线电机运转
  stepper2.run();  // 送线电机运转
}

// 暂停绕线
void pause() {
  stepper1.stop();
  stepper2.stop();
}

// 前向调整
void regulate_forward() {
  if (wiring_status.command_pulse == true) {
    store_status();
    stepper2.move(wiring_profile.regulate_delta * wiring_status.offset_multiple);  // 正向调整
    wiring_status.command_pulse = false;
  }
  if (stepper2.currentPosition() == wiring_profile.regulate_delta * wiring_status.offset_multiple) {
    stepper2.stop();
    restore_status();
  } else {
    stepper2.run();
  }
}

// 后向调整
void regulate_backward() {
  if (wiring_status.command_pulse == true) {
    store_status();
    stepper2.move(-wiring_profile.regulate_delta * wiring_status.offset_multiple);  // 反向调整
    wiring_status.command_pulse = false;
  }
  if (stepper2.currentPosition() == -wiring_profile.regulate_delta * wiring_status.offset_multiple) {
    stepper2.stop();
    restore_status();
  } else {
    stepper2.run();
  }
}

// 暂存当前送线电机的状态
void store_status() {
  wiring_status.current_position = stepper2.currentPosition();  // 暂存当前位置
  stepper2.setCurrentPosition(0);                               // 设置当前位置为调整偏移量的起点
  stepper2.setMaxSpeed(wiring_profile.regulate_speed);            // 设置偏移量调节速度
  stepper2.setAcceleration(wiring_profile.regulate_speed);        // 设置偏移量调节加速度
}

// 恢复送线电机的状态
void restore_status() {
  stepper2.setCurrentPosition(wiring_status.current_position);  // 恢复暂存的位置
  stepper2.setMaxSpeed(wiring_profile.feed_speed);              // 恢复绕线速度
  stepper2.setAcceleration(wiring_profile.feed_speed);          // 恢复绕线加速度
}
